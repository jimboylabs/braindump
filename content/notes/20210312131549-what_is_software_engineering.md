+++
title = "What is Software Engineering?"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Software Engineering at Google]({{< relref "20210312131615-software_engineering_at_google.md" >}}) [What is Software Engineering?](https://research.swtch.com/vgo-eng) [Modern Software Engineering]({{< relref "20220125080244-modern_software_engineering.md" >}})


## Notes {#notes}

_Software engineering_ adalah kegiatan programming yang terintegrasi dari waktu ke waktu.

_Programming_ adalah bagian dari _Software Engineering_.

_Software Engineering_ mencakup, pengembangan, modifikasi dan pemeliharan sebuah project software.


## Modern Software Engineering {#modern-software-engineering}

**Software engineering** is the application of an empirical, scientific approach to finding efficient, economic solutions to practical problems in software.
