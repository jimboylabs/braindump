+++
title = "How do we design a system for internationalization?"
author = ["Wayanjimmy"]
draft = false
+++

link
: [How do we design a system for internationalization](https://twitter.com/alexxubyte/status/1534564641904263168?s=20&t=BqZf0FiJtYTLcK11Qy4m7g)


## Notes {#notes}

Some example on how using separate npm package to maintain the localization files

-   <https://github.com/netless-io/flat/blob/main/packages/flat-i18n/package.json>
-   <https://github.com/netless-io/flat/blob/main/web/flat-web/src/utils/i18n.ts#L7-L32>
