+++
title = "Human Perception of Slowness"
author = ["Wayanjimmy"]
draft = false
+++

link
: [How Fast is Real-time? Human Perception and Technology](https://www.pubnub.com/blog/how-fast-is-realtime-human-perception-and-technology/)

in 1968 Robert Miller published his classic paper Response time in man-computer conversational transactions in which he described three different orders of magnitude of computer mainframe responsiveness:

> A response time of 100ms is perceived as instantaneous.
>
> Response time of 1 second or less are fast enough for users to feel they are interacting freely with the information.
>
> Response times greater than 10 seconds completely lose the user's attention.
