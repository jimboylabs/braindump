+++
title = "How to read JWT information using step cli"
author = ["Wayanjimmy"]
draft = false
+++

link
: [Step CLI Docs](https://smallstep.com/docs/step-cli/reference/crypto/jwt)

related
: [Command Line Tools]({{< relref "20220702135750-command_line_tools.md" >}})

<!--listend-->

```bash
$ echo $TOKEN | step crypto jwt inspect --insecure
{
  "header": {
    "alg": "ES256",
    "kid": "ZjGX97LmcflPolWvsoAWzC5WPWkNFFH3QdKLUW978hk",
    "typ": "JWT"
  },
  "payload": {
    "aud": "https://example.com",
    "exp": 1535242472,
    "iat": 1532564073,
    "iss": "joe@example.com",
    "nbf": 1532564073,
    "srv": "https://srv.example.com",
    "sub": "auth"
  },
  "signature": "DlSkxICjk2h1LarwJgXPbXQe7DwpLMOCvWp3I4GMcBP_5_QYPhVNBPQEeTKAUuQjYwlxZ5zVQnyp8ujvyf1Lqw"
}
```
