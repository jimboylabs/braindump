+++
title = "Explore Rancher Desktop as a local k8s dev"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Tinkering]({{< relref "20210503100841-tinkering.md" >}}) [Kubernetes]({{< relref "20210509131750-kubernetes.md" >}}) [POC in Ifood Project](https://gitlab.com/jimboylabs/ifood)


## Resources {#resources}

-   [Rancher Desktop and nerdctl for local k8s dev](https://itnext.io/rancher-desktop-and-nerdctl-for-local-k8s-dev-d1348629932a)
-   [Rancher Desktop Container Runtimes](https://blog.tilt.dev/2022/03/04/rancher-desktop-container-runtimes.html)
