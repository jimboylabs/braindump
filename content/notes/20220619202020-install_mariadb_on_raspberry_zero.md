+++
title = "Install MariaDB on Raspberry Zero"
author = ["Wayanjimmy"]
draft = false
+++

related
: [MySQL]({{< relref "20210415133341-mysql.md" >}})

link
: [How to Install MySQL on a RaspberryPI](https://betterprogramming.pub/how-to-install-mysql-on-a-raspberry-pi-ad3f69b4a094)
