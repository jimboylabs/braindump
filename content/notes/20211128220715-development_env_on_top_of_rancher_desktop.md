+++
title = "Development Env on top of Rancher Desktop"
author = ["Wayanjimmy"]
draft = false
+++

## Install MySQL {#install-mysql}

```nil
nerdctl container run -p 3306:3306 \
    --volume mysqldata:/var/lib/mysql \
    --env MYSQL_ROOT_PASSWORD=root -d mysql:5.7
```


## MyCLI {#mycli}

Install Mycli

```nil
sudo apt-get install mycli
```

Enter mysql shell

```nil
mycli -h localhost -u root -p root
```
