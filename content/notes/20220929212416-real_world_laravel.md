+++
title = "Real World Laravel"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Laravel]({{< relref "20210220134747-laravel.md" >}})

List of open-source projects based on Laravel, it's good to learn from open-source project out there as a reference for developing our own projects.


## Github {#github}

-   [Goodwork](https://github.com/Hasnayeen/goodwork)
-   [Akaunting](https://github.com/akaunting/akaunting)
-   [Laravel Shop](https://github.com/summerblue/laravel-shop)
-   [DaybyDayCRM](https://github.com/Bottelet/DaybydayCRM)
-   [Laracast Forum](https://github.com/laracasts/Lets-Build-a-Forum-in-Laravel)
-   [Free PMO](https://github.com/nafiesl/free-pmo)
-   [Laravel.io](https://github.com/laravelio/laravel.io)
-   [Academico](https://github.com/academico-sis/academico)


## Videos {#videos}

-   [Laravel.com Code Review](https://www.youtube.com/watch?v=ECYHgq62n6A)
-   [Laravel.io Code Structure](https://www.youtube.com/watch?v=eiIMbYbJHrA)
-   [Akaunting in Review](https://youtu.be/KUlwVA5MY3M)
-   [LaraBug Code Review](https://youtu.be/-nJ0a0Dx8Iw)
-   [DaybyDayCRM Code Review](https://youtu.be/bo7JMXGKlv8)
