# Use the official Hugo image as the base image
FROM docker.io/klakegg/hugo:ext-alpine as builder

ARG BASE_URL=http://localhost:8080

# Set the working directory
WORKDIR /app

# Copy the entire project to the working directory
COPY . .

RUN git submodule update --init --recursive

# Build the Hugo site
RUN hugo --gc --minify -b ${BASE_URL}

# Use a minimal nginx image to serve the static site
FROM docker.io/caddy:alpine

# Copy the generated site from the builder stage to the nginx html directory
COPY --from=builder /app/public /srv

# Expose port 80
EXPOSE 80

# Start nginx
CMD ["caddy", "file-server", "--listen", ":80", "--root", "/srv"]