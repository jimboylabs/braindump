+++
title = "Role and Influence: The IC trajectory beyond Staff"
author = ["Wayanjimmy"]
draft = false
+++

link
: [Leaddev](https://leaddev.com/leaddev-live/role-and-influence-ic-trajectory-beyond-staff)

related
: [Software Engineering]({{< relref "20220415173437-software_engineering.md" >}})


## The IC Path {#the-ic-path}

{{< figure src="/ox-hugo/20221006_213154_92YHli.png" >}}

1.  Junior Engineering
    Execute a task with supervision

2.  Senior Engineering
    Execute a task without supervision

3.  Staff
    Go from description to the tasks

4.  Senior Staff
    Go from well-defined problem area to the tasks

5.  Principal
    Understand an area and manage it

6.  Distinguishable+
    Find the problems and fix them


## Every job has four groups of skills {#every-job-has-four-groups-of-skills}

1.  The core **technical skill** (software engineering, UX, litigation, etc)
2.  Product management
3.  Project management
4.  People management


## Every team needs leadership of all four types. {#every-team-needs-leadership-of-all-four-types-dot}

Your combo shapes the kind of influence you have:

-   Just engineering: Hyperspecialist (and your influence wanes)
-   + Other tech skills (beyond end): Bridge and communicator
-   + Product mgmt: Become strategic leade, shape direction
-   + People mgmt: Become a mentor to the company, the person IC's turn to
-   + Project mgmt: Become an operational leader, drive execution
