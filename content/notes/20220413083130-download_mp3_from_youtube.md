+++
title = "Download Mp3 from Youtube"
author = ["Wayanjimmy"]
draft = false
+++

Example command to download mp3 files from a Youtube video

```bash
youtube-dl -i --extract-audio --audio-format mp3 --audio-quality 0 "PUT_YOUR_YOUTUBE_URL_HERE"
```
