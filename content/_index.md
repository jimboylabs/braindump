+++
title = "Index"
author = ["Wayan Jimmy"]
lastmod = 2020-05-29T21:44:03+08:00
type = "index"
draft = false
+++

## Hello! {#hello}

I'm [Jimmy](https://wayanjimmy.xyz/), this sites tends to be an improvement from the previous [notebook.wayanjimmy.xyz](http://notebook.wayanjimmy.xyz/).

Here are some notes that I've been collected.

### Life

- [Quotes](/notes/20210121152626-quotes/)
- [Finance](/notes/20210425134255-finance/)
- [Health](/notes/20210522172925-health/)
- [Philosophy](/notes/20210131181150-philosophy/)


### Uses

- [My Homelab](/notes/20210612122916-my_homelab/)
- [My Linux Machine](/notes/20210323204919-my_linux_machine/)
- [My Windows Machine](/notes/20211104074251-my_windows_machine/)


### Learn & Tinkering Labs

- [Git](/notes/20210217134705-git/)
- [Linux](/notes/20210502110347-linux/)
- [Golang](/notes/20201205165502-golang/)
- [Docker](/notes/20210518095808-docker/)
- [Ansible](/notes/20210807084603-ansible/)
- [Proxmox](/notes/20210509131657-proxmox/)
- [Tinkering](/notes/20210503100841-tinkering/)
- [Multipass](/notes/20210228151250-multipass/)
- [Doom Emacs](/notes/20201208184126-doom_emacs/)
- [Elasticsearch](/notes/20201221151118-elasticsearch/)
- [Software Engineering](/notes/20220415173437-software_engineering/)
- [Pi: Single Board Computer](/notes/20210420122611-pi_single_board_computer/)

### Resources

- [Books](/notes/20210504100738-books/)
- [Talks](/notes/20210511121448-talks/)
- [Course](/notes/20210818115002-course/)

click the full index to see all notes.

- [Full index](/notes/)
