+++
title = "Engineering The Practical Application of Science"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Modern Software Engineering]({{<relref "20220125080244-modern_software_engineering.md#" >}})

Software development is a process of discovery and exploration; therefore, to succeed at it, software engineers need to become experts **at learning**.

Humanity's best approach to learning is **science**, so we need to adopt the techniques and strategies of science and apply them to our problems.

This is often misunderstood to mean that we need to become physicists measuring things to unreasonable, in the context of software, levels of precision.

Engineering is more **pragmatic** than that.

What I mean when I say we should apply the techniques and strategies of science is that we should apply some pretty basic, but nevertheless extremely important, ideas.


## Characterize {#characterize}

Make an observation of the current state


## Hypothesize {#hypothesize}

Create a description, a theory that may explain your observation


## Predict {#predict}

Make a prediction based on your hypothesis


## Experiment {#experiment}

Test your prediction
