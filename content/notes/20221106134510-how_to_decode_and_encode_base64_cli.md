+++
title = "How to Decode and Encode base64 CLI"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Software Engineering]({{< relref "20220415173437-software_engineering.md" >}})


## Encode a string {#encode-a-string}

```bash
echo -n 'my-string' | base64
```


## Decode a string {#decode-a-string}

```bash
echo -n 'bXktc3RyaW5n' | base64 --decode
```
