+++
title = "Technical Writing"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Software Engineering]({{< relref "20220415173437-software_engineering.md" >}})

link
: [A Practical Guide to Writing Technical Specs](https://stackoverflow.blog/2020/04/06/a-practical-guide-to-writing-technical-specs/)


## Resources {#resources}

-   [Documentation System Divio](https://documentation.divio.com/)
