+++
title = "Assume you got only 1 vm to do everything"
author = ["Wayanjimmy"]
draft = false
+++

links
: [Twitter](https://twitter.com/AjeyGore/status/1477299345267445760)

related
: [Quotes]({{< relref "20210121152626-quotes.md" >}}) [Software Engineering]({{< relref "20220415173437-software_engineering.md" >}})

> If you are building a product, assume you got only 1 vm to do everything, including deploying on that vm. You will write minimal software with minimum complexity, beware of accidental complexity that you bring in just because it modern buzzwords.
>
> Ajey Gore
