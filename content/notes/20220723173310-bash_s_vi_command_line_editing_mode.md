+++
title = "Bash's  Vi Command  Line Editing Mode"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Linux]({{< relref "20210502110347-linux.md" >}})

link
: [Bash Vi Editing Mode Cheatsheet](https://catonmat.net/bash-vi-editing-mode-cheat-sheet)


## Notes {#notes}

Get into the **vi editing mode** type

```bash
set -o  vi
```
