+++
title = "Directus Up & Running"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Nocode Platform]({{<relref "20211003183408-nocode_platform.md#" >}})


## Install {#install}

```nil
npm init directus-project toko_onlen
```

```nil
cd toko_onlen
```

```nil
npx directus start
```
