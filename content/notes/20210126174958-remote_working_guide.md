+++
title = "Remote Working Guide"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Remote Working]({{<relref "20210216090747-remote_working.md#" >}})


## Articles {#articles}


### [Empathy Online](https://thoughtbot.com/blog/empathy-online) {#empathy-online}


### [Communicating Effectively in Remote Collaboration](https://worksmartandlevelup.substack.com/p/1-communicating-effectively-in-remote) {#communicating-effectively-in-remote-collaboration}


### [Kind.Engineering](https://kind.engineering/) {#kind-dot-engineering}


### [Managing Up](https://worksmartandlevelup.substack.com/p/managing-up?r=999ft&utm%5Fcampaign=post&utm%5Fmedium=email&utm%5Fsource=copy) {#managing-up}


### [Don't Mute, Get a Better Headset](https://ma.tt/2020/03/dont-mute-get-a-better-headset/) {#don-t-mute-get-a-better-headset}


## Podcasts {#podcasts}


### [GoTime: Why writing is important](https://changelog.com/gotime/164) {#gotime-why-writing-is-important}


## Videos {#videos}


### [Ergonomics Expert Explains How to Set Up Your Desk | WSJ](https://youtu.be/F8%5FME4VwTiw) {#ergonomics-expert-explains-how-to-set-up-your-desk-wsj}
