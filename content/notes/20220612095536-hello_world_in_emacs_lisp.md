+++
title = "Hello World in Emacs Lisp"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Emacs Lisp]({{< relref "20220612094751-emacs_lisp.md" >}})

link
: [Introduction to Emacs Lisp](https://youtu.be/RQK_DaaX34Q%7D)

<!--listend-->

```elisp
(+ 1 2)
```
