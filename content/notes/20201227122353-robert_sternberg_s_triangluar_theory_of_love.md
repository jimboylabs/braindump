+++
title = "Robert Sternberg's Triangular Theory of Love"
author = ["Wayanjimmy"]
draft = false
+++

links
: [Psych 101](https://www.goodreads.com/book/show/14618921-psych-101) [Triangular Theory of Love](https://www.instagram.com/p/Bzr8tmHHyWk)

In this 2004 theory, Robert Sternberg proposed that love could be broken
down into three parts: intimacy, passion, and commitment.


## Intimacy {#intimacy}

Closeness, supporting one another, sharing with one another, and feeling loved.


## Passion {#passion}

Feelings of sexual arousal and attraction, and euphoria. This is what drives two individuals together.


## Commitment {#commitment}

The desire to remain loyal to another person and stay in a long-term relationship.
