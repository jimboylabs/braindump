+++
title = "Connect to Multipass from WSL2"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Multipass]({{< relref "20210228151250-multipass.md" >}}) [My Windows Machine]({{< relref "20211104074251-my_windows_machine.md" >}})

link
: [Multipass Github Issue](https://github.com/canonical/multipass/issues/118#issuecomment-1158487763) [WSL Github Issue](https://github.com/microsoft/WSL/issues/4288#issuecomment-778790363)


## Notes {#notes}

From an admin PowerShell:

```nil
Set-NetIPInterface -ifAlias "vEthernet (WSL)" -Forwarding Enabled
Set-NetIPInterface -ifAlias "vEthernet (Default Switch)" -Forwarding Enabled
```

Makes sure to run as Administrator.
