+++
title = "My Windows Machine"
author = ["Wayanjimmy"]
draft = false
+++

## Resources {#resources}

-   [Windows Terminal](https://github.com/microsoft/terminal)
-   [Windows 10 Debloater](https://github.com/Sycnex/Windows10Debloater)
-   [WSL2](https://docs.microsoft.com/en-us/windows/wsl/install)
-   [Rancher Desktop](https://rancherdesktop.io/)
-   [Magpie](https://github.com/Blinue/Magpie)
-   [Emacs compile and install](https://gist.github.com/markbil/733e3ec9bb5e8345ae3cffb4d4c139e3)
-   [PowerToys](https://github.com/microsoft/PowerToys)
-   [Disable Windows Terminal Bell](https://maikeru.github.io/2021-03-21-disable-windows-terminal-bell/)
-   [Files Manager](https://github.com/files-community/Files)
-   [Fixing WSL2 Localhost Issue](https://abdus.dev/posts/fixing-wsl2-localhost-access-issue/)
-   [Optimizer](https://github.com/hellzerg/optimizer)
-   [10 Best Windows Productivity Apps in 2022](https://youtu.be/BQKfAxFgi-E)
-   [Limit WSL2 Memory using .wslconfig](https://docs.microsoft.com/en-us/windows/wsl/wsl-config#wslconfig)
