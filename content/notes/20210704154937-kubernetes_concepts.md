+++
title = "Kubernetes Concepts"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Kube Academy]({{< relref "20210704155301-kube_academy.md" >}}) [Kubernetes]({{< relref "20210509131750-kubernetes.md" >}})


## Control Loop {#control-loop}

1.  Act
2.  Observe
3.  Diff

{{< figure src="/ox-hugo/kubernetes-control-loop.png" >}}
