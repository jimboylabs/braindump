+++
title = "The Search for Admin Dashboard Generator"
author = ["Wayanjimmy"]
tags = ["Tinkering"]
draft = false
+++

related
: [Nocode Platform]({{< relref "20211003183408-nocode_platform.md" >}})


## Links {#links}

-   [Refine](https://github.com/pankod/refine)
-   [Directus](https://directus.io/)
-   [Orchid Software](https://orchid.software/)
-   [Filament Admin V2]({{< relref "20211211101123-filament_admin_v2.md" >}})
