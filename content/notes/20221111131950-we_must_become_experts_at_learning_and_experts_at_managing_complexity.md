+++
title = "We must become experts at learning and experts at managing complexity"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Modern Software Engineering]({{< relref "20220125080244-modern_software_engineering.md" >}})


## Become experts at learning {#become-experts-at-learning}

To become experts at learning, we need the following:

-   Iteration
-   Feedback
-   Incrementalism
-   Experimentation
-   Empiricism


## Become experts at managing complexity {#become-experts-at-managing-complexity}

To become experts at managing complexity, we need the following:

-   Modularity
-   Cohesion
-   Separation of Concerns
-   Abstraction
-   Loose Coupling


## Steer the software development using those 10 ideas above {#steer-the-software-development-using-those-10-ideas-above}

-   Testability
-   Deployability
-   Speed
-   Controlling the variables
-   Continous delivery
