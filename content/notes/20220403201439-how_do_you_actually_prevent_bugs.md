+++
title = "How do you actually prevent bugs?"
author = ["Wayanjimmy"]
draft = false
+++

related
: [How do you actually prevent bugs](https://nicolalindgren.com/2021/11/15/how-do-you-actually-prevent-bugs/) [Software Testing]({{< relref "20220403202051-software_testing.md" >}})


## Few things you can ask about during feature walkthroughs {#few-things-you-can-ask-about-during-feature-walkthroughs}

1.  Error scenarios - how will errors be handled; where will validation be done (backend or frontend) etc.
2.  Ask for the backend documentation and how things will be validated there, then make sure that things have stricter validation in the front end (where posibble),
    this helps prevent problems where the FE accepts data and you submit a form but the backend rejects it (e.g. mandatory fields, all mandatory fields the backend requires MUST be mandatory int the front end)
3.  Are there any mandatory fields we need to be consider?
4.  Are there any other features we expect this feature to be consistent with?
5.  Are there any dependencies within the app we need to be aware of?
6.  Are there any external dependencies (e.g. 3rd party ones that we need to be aware of?)
