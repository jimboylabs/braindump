+++
title = "System Design Interview"
author = ["Wayanjimmy"]
tags = ["Books"]
draft = false
+++

link
: [System Design Interview Amazon](https://www.amazon.com/System-Design-Interview-insiders-Second/dp/B08CMF2CQF/ref=sr_1_1?tag=gregdoesit-20&dchild=1&keywords=system%20design%20interview&qid=1604850753&sr=8-1&geniuslink=true)

related
: [Books]({{< relref "20210504100738-books.md" >}})
