+++
title = "Intel NUC for Developer Workstation"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Linux]({{< relref "20210502110347-linux.md" >}}) [NUC Hackintosh]({{< relref "20210227082512-nuc_hackintosh.md" >}})


## Links {#links}

-   [10th Gen Intel NUC Diassembly](https://youtu.be/vPxo0Y_P_F0)
-   [Intel NUC as a backend for development](https://dimamoroz.com/2021/03/09/intel-nuc-for-development/)
-   [Intel NUC 10i7FNH Frost Canyon Review](https://www.anandtech.com/show/15571/intel-nuc10i7fnh-frost-canyon-review)
