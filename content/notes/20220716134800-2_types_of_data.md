+++
title = "2 Types of Data"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Software Architecture The Hard Parts]({{< relref "20220716133710-software_architecture_the_hard_parts.md" >}})


## Operational Data {#operational-data}

Data yang digunakan dalam bisnis proses, seperti data penjualan, transaksi, gudang, dll.
Jika data ini bermasalah, akan mengganggu operasional bisnis. Biasanya melibatkan insert, update dan delete dalam sebuah database.


## Analytical Data {#analytical-data}

Data yang digunakan data scientist atau bisnis analis untuk melakukan prediksi, trending, dan proses bisnis inteligence lain-nya.

Jenis data ini bukan transaksional maupun relationsal, biasanya berupa graph database atau snapshot data. Jenis data ini tidak kritikal dalam operasi bisnis sehari-hari namun lebih berupa data untuk kepentingan menentukan arah bisnis dalam jangka panjang.
