+++
title = "Dead Letter Queue"
author = ["Wayanjimmy"]
draft = false
+++

link
: [A deadletter queue?](https://youtu.be/XNXbjWNsKAE)


## Notes {#notes}

adalah worker yang memproses queue yang mati, yang sudah melewati retry policy yang sebelumnya diproses oleh main worker
