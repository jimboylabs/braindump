+++
title = "Gofood POC with Filament Admin V2"
author = ["Wayanjimmy"]
draft = false
+++

link
: [Gitlab Source](https://gitlab.com/jimboylabs/mono/-/tree/main/20211211%5Fgofood) [Youtube Log](https://youtu.be/xYtay4moM-Q)

related
: [Tinkering]({{<relref "20210503100841-tinkering.md#" >}}) [Filament Admin V2]({{<relref "20211211101123-filament_admin_v2.md#" >}}) [Gofood POC queue library using Asynq]({{<relref "20211212163952-gofood_poc_queue_library_using_asynq.md#" >}})


## Description {#description}

User will be able to view list and search of food menus from several restaurants

1 restaurant will have several menu


## System Design {#system-design}

<https://excalidraw.com/#json=wzqIszCCQ8VF%5FetM7HNKU,vSPxih715uSLvflu28s2cA>

{{< figure src="/ox-hugo/20211212_gofood_poc_systemdesign.png" >}}


## Database Design {#database-design}


### Entities {#entities}

1.  Category
2.  Merchant
3.  Menu

{{< figure src="/ox-hugo/20211211_gofood_database_schema.png" >}}


## Tech Stack {#tech-stack}

1.  [Filament Admin V2]({{<relref "20211211101123-filament_admin_v2.md#" >}})
2.  Sqlite / [MySQL]({{<relref "20210415133341-mysql.md#" >}})


## Pages {#pages}


### Category Resource {#category-resource}

List page

{{< figure src="/ox-hugo/20211211_gofood_admin_listcategories.png" >}}

Create/Edit page

{{< figure src="/ox-hugo/20211211_gofood_admin_categoryform.png" >}}


### Merchant Resource {#merchant-resource}

List page

{{< figure src="/ox-hugo/20211211_gofood_admin_listmerchants.png" >}}

Create/Edit page

{{< figure src="/ox-hugo/20211211_gofood_admin_merchantform.png" >}}


### Menu Resource {#menu-resource}

List page

{{< figure src="/ox-hugo/20211211_gofood_admin_listmenu.png" >}}

Create/Edit page

{{< figure src="/ox-hugo/20211211_gofood_admin_menuform.png" >}}
