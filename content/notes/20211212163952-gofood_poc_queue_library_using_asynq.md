+++
title = "Gofood POC queue library using Asynq"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Gofood POC with Filament Admin V2]({{<relref "20211211101201-gofood_poc_with_filament_admin_v2.md#" >}}) [Tinkering]({{<relref "20210503100841-tinkering.md#" >}})


## Presequites {#presequites}

Make sure you instances of these services, working locally:

-   [MySQL]({{<relref "20210415133341-mysql.md#" >}})
-   [Elasticsearch up and running development mode]({{<relref "20211212141223-elasticsearch_up_and_running_development_mode.md#" >}})
-   [Kibana]({{<relref "20201221151206-kibana.md#" >}})
-   [Redis]({{<relref "20210202205541-redis.md#" >}})

I put the Dockerfiles that I use on [My Homelab]({{<relref "20210612122916-my_homelab.md#" >}}) repo


## Tasks {#tasks}

-   REST API CRUD menu
-   On CRUD event insert to [Elasticsearch]({{<relref "20201221151118-elasticsearch.md#" >}})
-   Implement asynq library
-   Compare with and without asynq with load testing


## How to {#how-to}


### Generate models code using xo {#generate-models-code-using-xo}

From the root project, create the `pkg/models/mysql` dir, then execute the command below.

```bash
xo schema 'mysql://root:root@127.0.0.1/gofood?parseTime=true' -o mysql
```

{{< figure src="/ox-hugo/20211212_gofood_models_xo.png" >}}
