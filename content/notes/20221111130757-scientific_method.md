+++
title = "Scientific Method"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Modern Software Engineering]({{< relref "20220125080244-modern_software_engineering.md" >}})


## Notes {#notes}

The scientific method that most of us learned about in school is described by Wikipedia as:

1.  Characterize: Make an observation of the current state.
2.  Hypothesize: Create a description, a theory that my explain your observation.
3.  Predict: Make a prediction based on your hypothesis.
4.  Experiment: Test your prediction.
