+++
title = "Organizing Go Project Layout"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Golang]({{<relref "20201205165502-golang.md#" >}})


[Repository Structure](https://peter.bourgon.org/go-best-practices-2016/#repository-structure)

[How do you structure your go apps?](https://youtu.be/1rxDzs0zgcE)

<https://github.com/netlify/gocommerce>

<https://github.com/disintegration/bebop>

<https://github.com/thockin/go-build-template>

<https://github.com/albertwidi/go-project-example>

<https://github.com/katzien/go-structure-examples>

<https://github.com/golang-standards/project-layout>

<https://github.com/sagikazarmark/modern-go-application>
