+++
title = "Elasticsearch up and running development mode"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Elasticsearch]({{<relref "20201221151118-elasticsearch.md#" >}})

Run elasticsearch easily in development mode


## Container using nerdctl {#container-using-nerdctl}


### Get the Dockerfile {#get-the-dockerfile}

Clone the [homelab](https://gitlab.com/jimboylabs/homelab) repo


### Build the image {#build-the-image}

Inside the homelab repo

```bash
cd containers/elasticsearch

nerdctl image build -t jimboylabs/elasticsearch:latest .
```


### Run the container {#run-the-container}

```bash
nerdctl container run -e "discovery.type=single-node" -v elasticsearch:/usr/share/elasticsearch/data \
    -p 9200:9200 -p 9300:9300 -d --name jim_es jimboylabs/elasticsearch:latest
```
