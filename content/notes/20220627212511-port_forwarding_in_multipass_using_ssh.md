+++
title = "Port Forwarding in Multipass using SSH"
author = ["Wayanjimmy"]
draft = false
+++

related
: [Multipass]({{< relref "20210228151250-multipass.md" >}})

link
: [Github Issue](https://github.com/canonical/multipass/issues/309#issuecomment-513594601) [A Visual Guide to SSH Tunnels](https://iximiuz.com/en/posts/ssh-tunnels/)


## Forward requests from multipass host (port 9000) to multipass instance (port 80) {#forward-requests-from-multipass-host--port-9000--to-multipass-instance--port-80}

```bash
sudo ssh \
      -i /var/root/Library/Application\ Support/multipassd/ssh-key/id_rsa \
      -L 9000:localhost:80 \
      multipass@<multipass instance ip>
```


## access multipass instance (port 80) from any box that can access multipass host {#access-multipass-instance--port-80--from-any-box-that-can-access-multipass-host}

```bash
curl <mutlipass host ip>:9000
```
