+++
title = "Golang Interface Checks"
author = ["Wayanjimmy"]
draft = false
+++

link
: [Effective Go](https://go.dev/doc/effective_go)

related
: [Golang]({{< relref "20201205165502-golang.md" >}})


## Notes {#notes}

To guarantee that the implementation is correct, a global declaration using the blank identifier can be used in the package:

In this declaration, the assignment involving a conversion of a \*RawMessage to a Marshaler requires that \*RawMessage implements Marshaler, and that property
will be checked at compile time. Should the json.

Marshaler interface change, this package will no longer compile and we will be on notice that it needs to be
updated.

```go
var _ json.Marshaler = (*RawMessage)(nil)
```

The appearance of the blank identifier in this construct indicates that the declaration exists only for the type checking, not to create a variable. Don't do this for every
type that satisfies an interface, though.

By convention, such declarations are only used when there are no static conversions already present in the code, which is a
rare event.
